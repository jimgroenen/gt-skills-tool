Skills = new Mongo.Collection("skills");
Grades = new Mongo.Collection("grades");

if (Meteor.isServer) {
  Meteor.startup(function () {
    Skills.remove({});
    if (Skills.find().count() === 0) {

    Skills.insert({description:"skill 1"});
    Skills.insert({description:"skill 2"});
    Skills.insert({description:"skill 3"});
    Skills.insert({description:"skill 4"});
    Skills.insert({description:"skill 5"});
  }
  }
)};


if (Meteor.isClient) {
  // This code only runs on the client
  Template.body.helpers({
    skills: function () {
      return Skills.find({}, {sort: {createdAt: -1}});

    }
  });

  Template.body.events({
    'click .dropdown li' : function(event){
        console.log(event.target.data.)
      }
  })

};



